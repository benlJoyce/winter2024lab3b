import java.util.Scanner;
public class VirtualPetApp{
	public static void main(String[] args){
		Scanner scanner = new Scanner(System.in);
		Dog[] groupOfDogs = new Dog[4];
		
		for(int i = 0; i < groupOfDogs.length; i++){
			System.out.println("\nInsert details of your dog: ");
			
			System.out.println("Enter your dog's name");
			String dogName = scanner.nextLine();
			
			System.out.println("Enter your dog's age");
			int dogAge = Integer.parseInt(scanner.nextLine());
			
			System.out.println("Enter the trick your dog can do :D");
			String dogTrick = scanner.nextLine();
			
			groupOfDogs[i] = new Dog(dogName, dogAge, dogTrick);
			groupOfDogs[i].calculateAge();
			System.out.println(groupOfDogs[i].doTrick());
			
		
		}
		
		Dog lastDog = groupOfDogs[groupOfDogs.length - 1];
		System.out.println("\nDog's name: " + lastDog.dogName);
		System.out.println("Dog's age: " + lastDog.ageInDogYears);
		System.out.println("Dogs hidden talent hehe : " + lastDog.dogTrick);
		
		groupOfDogs[0].calculateAge();
		groupOfDogs[0].doTrick();
		
		
	}
}