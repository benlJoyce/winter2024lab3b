public class Dog{
	public String dogName;
	public int dogAge;
	public String dogTrick;
	public int ageInDogYears;
	
	public Dog(String n, int a, String b){
		dogName = n;
		dogAge = a;
		dogTrick = b;

	
	}
	
	//method that calculates the age of the dog from human years to dog years
	public void calculateAge(){
		ageInDogYears = dogAge * 7;
		System.out.println("\nYour dogs age is: " + ageInDogYears + "years old.");
	}
	
	// doggo does a trick !
	public String doTrick(){
		return "\n" + "***" + dogName + " performed " + dogTrick + "  ^_^" + "***";
	}
}